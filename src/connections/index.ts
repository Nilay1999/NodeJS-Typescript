import env from "dotenv";
import { createConnection } from "typeorm";
env.config();

export const connection = createConnection({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "root",
    database: process.env.DB_NAME as string,
    entities: ["src/models/**/*.ts"],
    synchronize: true,
});
