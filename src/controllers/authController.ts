import { Request, Response } from "express";
import { UserModel } from "../models/user.model";
import jwt from "jsonwebtoken";
import env from "dotenv";
env.config();

const JWT_SECRET = process.env.JWT_SECRET;

class AuthController {
    async login(req: Request, res: Response) {
        const { email, password } = req.body;

        if (!email || !password) {
            return res
                .status(401)
                .json({ message: "Please enter all fields !" });
        } else {
            try {
                const user = await UserModel.findOne({
                    where: { email: email },
                });
                if (!user) {
                    return res.status(404).json({ message: "User not found" });
                } else {
                    const token = jwt.sign(
                        { user: user },
                        JWT_SECRET as string
                    );
                    return res.json({ message: "Logged In !", token: token });
                }
            } catch (error) {
                res.json(error);
            }
        }
    }
}

export default new AuthController();
