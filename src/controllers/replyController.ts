import { Comment } from "../models/comment.model";
import { Request, Response } from "express";
import { Reply } from "../models/reply.model";
import { In } from "typeorm";

class ReplyController {
    async getCommentsByUserId(req: Request, res: Response) {
        try {
            const { userId } = req.params;
            const userCommentData = await Comment.find({
                where: {
                    commentedBy: {
                        id: userId,
                    },
                },
            });
            if (!userCommentData) {
                return res.status(404).json({
                    message: "Comments not found",
                });
            } else {
                return res.status(200).json({
                    data: userCommentData,
                    message: "Comments found",
                });
            }
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async getAllComments(req: Request, res: Response) {
        try {
            const allComments = await Comment.find({
                relations: ["commentedBy", "reply", "reply.commentedBy"],
            });
            if (!allComments) {
                return res.status(404).json({
                    message: "Comments not found",
                });
            } else {
                return res.status(200).json({
                    data: allComments,
                    message: "Comments found",
                });
            }
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async addComment(req: Request, res: Response) {
        try {
            const { commentContent, userId } = req.body;
            const commentData = await Comment.create({
                commentContent: commentContent,
                commentedBy: { id: parseInt(userId) },
            });
            await commentData.save();
            return res.json({
                data: commentData,
            });
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async addReply(req: Request, res: Response) {
        try {
            const { parentId } = req.params;
            const { replyContent, userId } = req.body;
            const replyData = await Reply.create({
                commentContent: replyContent,
                commentedBy: { id: userId },
                parent: {
                    id: parseInt(parentId),
                },
            });
            await replyData.save();
            return res.json({
                data: replyData,
            });
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async addUpvotesToComment(req: Request, res: Response) {
        try {
            const { commentId } = req.params;
            const { userId } = req.body;

            const commentData = await Comment.createQueryBuilder()
                .update(Comment)
                .set({
                    upvotes: () => "upvotes + 1",
                })
                .where("id = :id", { id: commentId })
                .execute();

            return res.status(200).json({
                message: "upvoted successfully",
                data: commentData,
            });
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }
    async addUpvotesToReply(req: Request, res: Response) {
        try {
            const { replyId } = req.params;
            const replyData = await Reply.createQueryBuilder()
                .update(Reply)
                .set({ upvotes: () => "upvotes + 1" })
                .where("id = :id", { id: replyId })
                .execute();
            return res.status(200).json({
                message: "upvoted successfully",
                data: replyData,
            });
        } catch (error) {
            return res.status(200).json({
                message: "upvoted successfully",
            });
        }
    }
}

export default new ReplyController();
