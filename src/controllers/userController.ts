import { Request, Response } from "express";
import { UserModel } from "../models/user.model";

class UserController {
    async registerUser(req: Request, res: Response) {
        try {
            const { email, username, password } = req.body;
            if (!email || !username || !password) {
                return res.json({ message: "Please enter all fields" });
            } else {
                const isUserExists = await UserModel.findOne({
                    where: { email: email },
                });
                if (isUserExists) {
                    return res.json({ message: "User already exists !" });
                } else {
                    const user = await UserModel.save({
                        ...req.body,
                    });
                    return res.json({
                        data: user,
                        message: "Successfully added data",
                    });
                }
            }
        } catch (error) {
            res.json(error);
        }
    }

    async getUser(req: Request, res: Response) {
        try {
            const { id } = req.params;
            const user = await UserModel.findOne(id);
            if (!user) {
                return res.json({ message: "User not found" });
            } else {
                return res.json(res.locals.data.user);
            }
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async getUsers(req: Request, res: Response) {
        try {
            const users = await UserModel.find({});
            return res.json(users);
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async editUser(req: Request, res: Response) {
        try {
            const { email, username } = req.body;
            const { id } = req.params;
            const user = await UserModel.findOne(id);
            if (!user) {
                res.json({ message: "Can't find User" });
            } else {
                const updatedUser = await UserModel.createQueryBuilder()
                    .update(UserModel)
                    .set({ email: email, username: username })
                    .where("id = :id", { id: id })
                    .execute();
                return res.json(updatedUser);
            }
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }

    async deleteUser(req: Request, res: Response) {
        try {
            await UserModel.createQueryBuilder()
                .delete()
                .from(UserModel)
                .where("id = :id", { id: 1 })
                .execute();

            res.json({ message: "User deleted" });
        } catch (error) {
            return res.status(400).json({
                message: error,
            });
        }
    }
}

export default new UserController();
