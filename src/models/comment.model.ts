import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from "typeorm";
import { Reply } from "./reply.model";
import { UserModel } from "./user.model";

@Entity()
export class Comment extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    commentContent!: string;

    @ManyToOne(() => UserModel, (user) => user.comments)
    @JoinColumn({ name: "commentedBy" })
    commentedBy!: UserModel;

    @Column({ default: 0 })
    upvotes?: number;

    @OneToMany(() => Reply, (reply) => reply.parent)
    reply?: Reply[];

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;
}
