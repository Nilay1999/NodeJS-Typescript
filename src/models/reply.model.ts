import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from "typeorm";
import { Comment } from "./comment.model";
import { UserModel } from "./user.model";

@Entity()
export class Reply extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    commentContent!: string;

    @ManyToOne(() => UserModel, (user) => user.comments)
    commentedBy!: UserModel;

    @Column({ default: 0 })
    upvotes?: number;

    @ManyToOne(() => Comment, (parent) => parent.reply)
    @JoinColumn({ name: "parent" })
    parent?: Comment;

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;

    @Column("int", { array: true, nullable: true, default: {} })
    upvotedBy?: number[];
}
