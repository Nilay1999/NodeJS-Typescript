import express from "express";
import cors from "cors";
import routes from "./routes";
import env from "dotenv";
import { connection } from "./connections";
env.config();
const PORT = process.env.PORT || 3000;
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api", routes);
app.get("/", (req, res) => {
    res.send("Server running");
});
connection
    .then(() => {
        app.listen(PORT, () => {
            try {
                console.log("Connected to Database ");
                console.log(`Server running on ${PORT}`);
            } catch (error) {
                console.error("Unable to connect to MySql");
            }
        });
    })
    .catch((error) => console.log(error));
