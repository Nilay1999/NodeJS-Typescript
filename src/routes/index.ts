import express from "express";
import * as userRoutes from "./userRoutes";
import * as replyRoutes from "./replyRoutes";
const routes = express.Router();

routes.use("/user-api", userRoutes.default);
routes.use("/reply-api", replyRoutes.default);
export default routes;
