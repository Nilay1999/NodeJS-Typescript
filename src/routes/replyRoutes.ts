import express from "express";
import replyController from "../controllers/replyController";

const routes = express.Router();

routes.get("/", replyController.getAllComments);
routes.get("/:userId", replyController.getCommentsByUserId);
routes.post("/addComment/", replyController.addComment);
routes.post("/addReply/:parentId", replyController.addReply);
routes.put("/upvote-comment/:commentId", replyController.addUpvotesToComment);
routes.put("/upvote-reply/:replyId", replyController.addUpvotesToReply);

export default routes;
