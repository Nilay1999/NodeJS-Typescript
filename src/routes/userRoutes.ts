import express from "express";
import userController from "../controllers/userController";
import authenticate from "../middleware/authenticate";
const routes = express.Router();

routes.get("/", userController.getUsers);
routes.get("/:id", userController.getUser);
routes.post("/", userController.registerUser);
routes.put("/:id", userController.editUser);
routes.delete("/:id", userController.deleteUser);

export default routes;
