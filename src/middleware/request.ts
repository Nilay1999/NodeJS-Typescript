import { Request } from "express";
import { UserModel } from "../models/user.model";

interface RequestWithUser extends Request {
    user: UserModel;
}

export default RequestWithUser;
