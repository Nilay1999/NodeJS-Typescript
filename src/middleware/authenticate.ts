import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import env from "dotenv";
const JWT_SECRET = process.env.JWT_SECRET;
env.config();

const authenticate = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = req.header("Authorization");
    if (!token) {
        return res.json({ message: "Please provide token" });
    } else {
        jwt.verify(token, JWT_SECRET as string, async (error, decoded) => {
            if (error) {
                res.json({ message: "Authentication failed" });
            } else {
                res.locals.data = decoded;
                next();
            }
        });
    }
};

export default authenticate;
